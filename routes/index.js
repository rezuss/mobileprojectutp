const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const fb = require('../firebase');
const saltedMd5 = require('salted-md5');
const path = require('path');
const multer = require('multer');
const upload = multer({
    dest: 'uploads/',
    storage: multer.memoryStorage()
});

var currentUser = fb.firebase.auth().currentUser;
var users = fb.users;
var posts_list = fb.posts_list;

// Home
router.get('/', (req, res) => {
    res.render('home', { currentUser, users });
});

// Register
// router.get('/register', (req, res) => {
//     res.render('auth/register', { currentUser });
// });

// router.post('/register', [
//     check('displayName')
//         .isLength({ min: 3 })
//         .withMessage('Nazwa musi mieć min. 3 znaki.'),
//     check('email')
//         .isLength({ min: 3 })
//         .withMessage('Proszę podać poprawny email.'),
//     check('password')
//         .isLength({ min: 6 })
//         .withMessage('Hasło musi mieć min. 6 znaków.'),
//     check('password2').custom((value, { req }) => {
//         if (value !== req.body.password)
//             throw new Error('Hasła muszą być takie same.');
//         return true;
//     })
// ], (req, res) => {
//     const errors = validationResult(req);
//     if (errors.isEmpty()) {
//         fb.admin.auth().createUser({
//             email: req.body.email,
//             password: req.body.password,
//             displayName: req.body.displayName
//         }).then((userRecord) => {
//             users.push(userRecord.toJSON());
//             console.log('Zarejestrowano');
//             currentUser = fb.firebase.auth().currentUser;
//             res.render('home', {
//                 currentUser,
//                 users,
//                 registered: true
//             });
//         }).catch((error) => {
//             var errors = [];
//             switch (error.code) {
//                 case 'auth/email-already-in-use':
//                     errors.push({ param: 'email', msg: 'Ten email jest już w użyciu.' });
//                     break;
//                 case 'auth/invalid-email':
//                     errors.push({ param: 'email', msg: 'Nieprawidłowy adres email.' });
//                     break;
//                 case 'auth/operation-not-allowed':
//                     errors.push({ param: 'email', msg: 'Logowanie zablokowane, skontaktuj się z administratorem Firebase.' });
//                     break;
//                 case 'auth/weak-password':
//                     errors.push({ param: 'password', msg: 'Za słabe hasło, min. 6 znaków.' });
//                     break;
//                 default:
//                     errors.push({ param: 'email', msg: 'Nieznany błąd: ' + error.message });
//                     break;
//             }
//             res.render('auth/register', {
//                 currentUser,
//                 errors: errors,
//                 data: req.body
//             });
//         });
//     } else {
//         res.render('auth/register', {
//             currentUser,
//             errors: errors.array(),
//             data: req.body
//         });
//     }
// });

// Login
router.get('/login', (req, res) => {
    res.render('auth/login', { currentUser });
});

router.post('/login', [
    check('email')
        .isLength({ min: 3 })
        .withMessage('Proszę podać poprawny email.'),
    check('email').trim()
        .custom((value) => {
            if (value !== 'admin@gmail.com')
                throw new Error('To konto nie ma dostępu do panelu.');
            return true;
        }),
    check('password')
        .isLength({ min: 6 })
        .withMessage('Hasło musi mieć min. 6 znaków.')
], (req, res) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        fb.firebase.auth().signInWithEmailAndPassword(req.body.email, req.body.password)
            .then((user) => {
                console.log("Zalogowano");
                currentUser = fb.firebase.auth().currentUser;
                res.redirect('/');
            }).catch((error) => {
                var errors = [];
                switch (error.code) {
                    case 'auth/user-disabled':
                        errors.push({ param: 'email', msg: 'To konto zostało zablokowane.' });
                        break;
                    case 'auth/invalid-email':
                        errors.push({ param: 'email', msg: 'Nieprawidłowy adres email.' });
                        break;
                    case 'auth/user-not-found':
                        errors.push({ param: 'email', msg: 'Nie znaleziono takiego użytkownika.' });
                        break;
                    case 'auth/wrong-password':
                        errors.push({ param: 'password', msg: 'Niepoprawne hasło.' });
                        break;
                    default:
                        errors.push({ param: 'email', msg: 'Nieznany błąd: ' + error.message });
                        break;
                }
                res.render('auth/login', {
                    currentUser,
                    errors: errors,
                    data: req.body
                });
            });
    } else {
        res.render('auth/login', {
            currentUser,
            errors: errors.array(),
            data: req.body
        });
    }
});

// Logout
router.get('/logout', (req, res) => {
    if (currentUser == null) {
        res.redirect('/login');
    } else {
        res.render('auth/logout', { currentUser, message: 'Pomyślnie wylogowano z konta.' });
    }
});

router.post('/logout', (req, res) => {
    if (currentUser) {
        fb.firebase.auth().signOut().then(() => {
            console.log('Wylogowano');
            currentUser = fb.firebase.auth().currentUser;
            res.redirect('/logout');
        }).catch((error) => {
            res.render('auth/logout', {
                currentUser,
                message: 'Błąd: ' + error.message
            });
        });
    }
});

// Users
router.get('/users', (req, res) => {
    if (currentUser == null) {
        res.redirect('/login');
    } else {
        res.render('users', { currentUser, users });
    }
});

// Add user
router.get('/adduser', (req, res) => {
    if (currentUser == null) {
        res.redirect('/login');
    } else {
        res.render('user/adduser', { currentUser });
    }
});

router.post('/adduser', [
    check('displayName')
        .isLength({ min: 3 })
        .withMessage('Nazwa musi mieć min. 3 znaki.'),
    check('email')
        .isLength({ min: 3 })
        .withMessage('Proszę podać poprawny email.'),
    check('password')
        .isLength({ min: 6 })
        .withMessage('Hasło musi mieć min. 6 znaków.'),
    check('password2').custom((value, { req }) => {
        if (value !== req.body.password)
            throw new Error('Hasła muszą być takie same.');
        return true;
    })
], (req, res) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        fb.admin.auth().createUser({
            email: req.body.email,
            password: req.body.password,
            displayName: req.body.displayName
        }).then((userRecord) => {
            users.push(userRecord.toJSON());
            console.log('Utworzono użytkownika');
            currentUser = fb.firebase.auth().currentUser;
            res.redirect('/users');
        }).catch((error) => {
            var errors = [];
            switch (error.code) {
                case 'auth/email-already-in-use':
                    errors.push({ param: 'email', msg: 'Ten email jest już w użyciu.' });
                    break;
                case 'auth/email-already-exists':
                    errors.push({ param: 'email', msg: 'Ten email jest już w użyciu.' });
                    break;
                case 'auth/invalid-email':
                    errors.push({ param: 'email', msg: 'Nieprawidłowy adres email.' });
                    break;
                case 'auth/operation-not-allowed':
                    errors.push({ param: 'email', msg: 'Logowanie zablokowane, skontaktuj się z administratorem Firebase.' });
                    break;
                case 'auth/weak-password':
                    errors.push({ param: 'password', msg: 'Za słabe hasło, min. 6 znaków.' });
                    break;
                default:
                    errors.push({ param: 'email', msg: 'Nieznany błąd: ' + error.message });
                    break;
            }
            res.render('user/adduser', {
                currentUser,
                errors: errors,
                data: req.body
            });
        });
    } else {
        res.render('user/adduser', {
            currentUser,
            errors: errors.array(),
            data: req.body
        });
    }
});

// Settings/Edit user
router.get('/edituser/:id', (req, res) => {
    if (currentUser == null) {
        res.redirect('/login');
    } else {
        const id = req.params.id;
        const user = users[id];
        if (user) {
            res.render('user/edituser', {
                currentUser,
                user,
                id,
                users
            });
        } else {
            res.redirect('/users');
        }
    }
});

router.post('/edituser/:id', (req, res) => {
    const id = req.params.id;
    const user = users[id];
    if (user) {
        var errors = [];
        var updateName = false;
        var updateEmail = false;
        var updatePassword = false;

        if (req.body.displayName.length >= 3) {
            updateName = true;
        } else {
            errors.push({ param: 'displayName', msg: 'Nazwa musi mieć min. 3 znaki.' });
        }

        if (req.body.email.length >= 3) {
            updateEmail = true;
            users.forEach(u => {
                if (u.email.toLowerCase() === req.body.email.toLowerCase()) {
                    if (user.uid !== u.uid) {
                        errors.push({ param: 'email', msg: 'Ten email jest już w użyciu.' });
                        updateEmail = false;
                    }
                }
            });
        } else {
            errors.push({ param: 'email', msg: 'Prosze podać poprawny email.' });
        }

        if (req.body.password.length !== 0) {
            if (req.body.password.length >= 6) {
                if (req.body.password === req.body.password2) {
                    updatePassword = true;
                } else {
                    errors.push({ param: 'password2', msg: 'Hasła muszą być takie same.' });
                }
            } else {
                errors.push({ param: 'password', msg: 'Hasło musi mieć min. 6 znaków.' });
            }
        }

        if (errors.length === 0) {
            if (updateName && updateEmail) { //name & email
                fb.admin.auth().updateUser(user.uid, {
                    displayName: req.body.displayName,
                    email: req.body.email
                }).then(userRecord => {
                    users[id] = userRecord;
                    currentUser = fb.firebase.auth().currentUser;
                    if (users[id].uid == currentUser.uid)
                        currentUser = users[id];
                    console.log('Zaktualizowano nazwę i email');
                    res.redirect('/users');
                }).catch(error => {
                    console.log(error);
                });
            } else if (updateName && !updateEmail) { //name
                fb.admin.auth().updateUser(user.uid, {
                    displayName: req.body.displayName
                }).then(userRecord => {
                    users[id] = userRecord;
                    currentUser = fb.firebase.auth().currentUser;
                    if (users[id].uid == currentUser.uid)
                        currentUser = users[id];
                    console.log('Zaktualizowano nazwę');
                    res.redirect('/users');
                }).catch(error => {
                    console.log(error);
                });
            } else if (!updateName && updateEmail) { //email
                fb.admin.auth().updateUser(user.uid, {
                    email: req.body.email
                }).then(userRecord => {
                    users[id] = userRecord;
                    currentUser = fb.firebase.auth().currentUser;
                    if (users[id].uid == currentUser.uid)
                        currentUser = users[id];
                    console.log('Zaktualizowano email');
                    res.redirect('/users');
                }).catch(error => {
                    console.log(error);
                });
            }
            if (updatePassword) {
                fb.admin.auth().updateUser(user.uid, {
                    password: req.body.password,
                }).then(userRecord => {
                    console.log('Zaktualizowano hasło');
                }).catch(error => {
                    console.log(error);
                });
                fb.firebase.auth().signOut().then(() => {
                    currentUser = fb.firebase.auth().currentUser;
                    console.log('Wylogowano');
                    res.redirect('/login');
                }).catch(error => {
                    console.log(error);
                });
            }
        } else {
            res.render('user/edituser', {
                currentUser,
                user,
                id,
                users,
                errors,
                data: req.body
            });
        }
    } else {
        res.redirect('/users');
    }
});

// Delete user
router.get('/deleteuser/:id', (req, res) => {
    if (currentUser == null) {
        res.redirect('/login');
    } else {
        const id = req.params.id;
        const user = users[id];
        if (user) {
            res.render('user/deleteuser', {
                currentUser,
                user,
                id,
                users
            });
        } else {
            res.redirect('/users');
        }
    }
});

router.post('/deleteuser/:id', (req, res) => {
    const id = req.params.id;
    const user = users[id];
    if (user) {
        fb.admin.auth().deleteUser(user.uid)
            .then(function () {
                users.splice(id, 1);
                console.log('Usunięto użytkownika');
                posts_list.forEach(post => {
                    if (post.user == user.uid) {
                        fb.firebase.database().ref('posts/' + post.id).remove(error => {
                            if (error) {
                                console.log(error);
                            } else {
                                posts_list.splice(id, 1);
                                for (var i = 0; i < posts_list.length; i++) {
                                    for (var j = 0; j < posts_list.length; j++) {
                                        if (i != j) {
                                            if (posts_list[i].id == posts_list[j].id) {
                                                posts_list.splice(j, 1);
                                            }
                                        }
                                    }
                                }
                            }
                        });
                    }
                });
                console.log('Usunięto wpisy użytkownika');
                res.redirect('/users');
            }).catch((error) => {
                console.log(error);
            });
    } else {
        res.redirect('/users');
    }
});

// Posts
router.get('/posts', (req, res) => {
    if (currentUser == null) {
        res.redirect('/login');
    } else {
        res.render('posts', { currentUser, users, posts_list });
    }
});

// Add post
router.get('/addpost', (req, res) => {
    if (currentUser == null) {
        res.redirect('/login');
    } else {
        res.render('post/addpost', { currentUser, users });
    }
});

router.post('/addpost', upload.single('file'), [
    check('title')
        .isLength({ min: 3 })
        .withMessage('Tytuł musi mieć min. 3 znaki.'),
    check('description')
        .isLength({ min: 6 })
        .withMessage('Opis musi mieć min. 6 znaków.'),
    check('userselect').custom((value) => {
        if (value === 'Wybierz użytkownika')
            throw new Error('Wybierz autora wpisu.');
        return true;
    })
], (req, res) => {
    const validation = validationResult(req);
    if (validation.isEmpty()) {
        const errors = [];
        const extensions = ['.png', '.jpg', '.jpeg'];
        if (req.file && extensions.includes(path.extname(req.file.originalname).toLowerCase())) {
            // Upload image
            const name = saltedMd5(req.file.originalname, 'SUPER-S@LT!');
            const fileName = name + path.extname(req.file.originalname);
            fb.bucket.file(fileName).createWriteStream().end(req.file.buffer);
            console.log('Wrzucono plik');
            const url = 'https://firebasestorage.googleapis.com/v0/b/mobileprojectutp.appspot.com/o/' + fileName + '?alt=media';

            // Add new post
            var date_ob = new Date();
            var day = ("0" + date_ob.getDate()).slice(-2);
            var month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
            var year = date_ob.getFullYear();
            var currentDate = day + '/' + month + '/' + year;
            fb.postsRef.push({
                title: req.body.title,
                description: req.body.description,
                image: url,
                coordinates: { x: 'Server Test', y: 'Server Test' },
                date: currentDate,
                user: req.body.userselect
            });
            console.log('Dodano wpis')
            for (var i = 0; i < posts_list.length; i++) {
                for (var j = 0; j < posts_list.length; j++) {
                    if (i != j) {
                        if (posts_list[i].id == posts_list[j].id) {
                            posts_list.splice(j, 1);
                        }
                    }
                }
            }
            res.redirect('/posts');
        } else {
            errors.push({ param: 'file', msg: 'Proszę załączyć zdjęcie w formacie JPG, JPEG lub PNG.' });
            res.render('post/addpost', {
                currentUser,
                users,
                errors,
                data: req.body
            });
        }
    } else {
        res.render('post/addpost', {
            currentUser,
            users,
            errors: validation.array(),
            data: req.body
        });
    }
});

// Edit post
router.get('/editpost/:id', (req, res) => {
    if (currentUser == null) {
        res.redirect('/login');
    } else {
        const id = req.params.id;
        const post = posts_list[id];
        if (post) {
            res.render('post/editpost', {
                currentUser,
                post,
                id
            });
        } else {
            res.redirect('/posts');
        }
    }
});

router.post('/editpost/:id', [
    check('title')
        .isLength({ min: 3 })
        .withMessage('Tytuł musi mieć min. 3 znaki.'),
    check('description')
        .isLength({ min: 6 })
        .withMessage('Opis musi mieć min. 6 znaków.'),
], (req, res) => {
    const errors = validationResult(req);
    const id = req.params.id;
    const post = posts_list[id];
    if (post) {
        if (errors.isEmpty()) {
            fb.firebase.database().ref('posts/' + post.id).update({
                title: req.body.title,
                description: req.body.description
            }, error => {
                if (error) {
                    console.log(error);
                } else {
                    posts_list.splice(id, 1);
                    for (var i = 0; i < posts_list.length; i++) {
                        for (var j = 0; j < posts_list.length; j++) {
                            if (i != j) {
                                if (posts_list[i].id == posts_list[j].id) {
                                    posts_list.splice(j, 1);
                                }
                            }
                        }
                    }
                    console.log('Zaktualizowano wpis');
                    res.redirect('/posts');
                }
            });
        } else {
            res.render('post/editpost', {
                currentUser,
                post,
                id,
                errors: errors.array(),
                data: req.body
            });
        }
    } else {
        res.redirect('/posts');
    }
});

// Delete post
router.get('/deletepost/:id', (req, res) => {
    if (currentUser == null) {
        res.redirect('/login');
    } else {
        const id = req.params.id;
        const post = posts_list[id];
        if (post) {
            res.render('post/deletepost', {
                currentUser,
                users,
                post,
                id
            });
        } else {
            res.redirect('/posts');
        }
    }
});

router.post('/deletepost/:id', (req, res) => {
    const id = req.params.id;
    const post = posts_list[id];
    if (post) {
        fb.firebase.database().ref('posts/' + post.id).remove(error => {
            if (error) {
                console.log(error);
            } else {
                posts_list.splice(id, 1);
                for (var i = 0; i < posts_list.length; i++) {
                    for (var j = 0; j < posts_list.length; j++) {
                        if (i != j) {
                            if (posts_list[i].id == posts_list[j].id) {
                                posts_list.splice(j, 1);
                            }
                        }
                    }
                }
                console.log('Usunięto wpis');
                res.redirect('/posts');
            }
        });
    } else {
        res.redirect('/posts');
    }
});

// Google maps
router.get('/google/:coordinates', function (req, res) {
    const coordinates = req.params.coordinates;
    const coord = coordinates.split(' ');
    res.redirect('https://www.google.com/maps/search/' + coord[0] + '+' + coord[1]);
});

// Default route
router.get('*', function (req, res) {
    res.redirect('/');
});

router.post('*', function (req, res) {
    res.redirect('/');
});

module.exports = router;