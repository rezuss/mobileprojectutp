const firebase = require('firebase');
const admin = require('firebase-admin');
require('firebase/storage');

// Firebase configs
const firebaseConfig = {
    apiKey: "AIzaSyDrM2rHtr63tkcrY0r0eoGUeJwRx7uGvHM",
    authDomain: "mobileprojectutp.firebaseapp.com",
    databaseURL: "https://mobileprojectutp-default-rtdb.firebaseio.com",
    projectId: "mobileprojectutp",
    storageBucket: "mobileprojectutp.appspot.com",
    messagingSenderId: "516087072054",
    appId: "1:516087072054:web:58e6eede299fbe7bbb5f97",
    measurementId: "G-KBMCC3JKJ1"
};

const adminConfig = {
    "type": "service_account",
    "project_id": "mobileprojectutp",
    "private_key_id": "9ce474802bcd871846d0bc504682f60ca6634495",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDR5SXC/TzcowCr\nuxoFf6HTaXewBZ98TeGlZmCrv6PLnIYPo7Gj3WXno8I/2jf44oS4s1EMRhqB4s1X\neQG9lSnuLqn4knoy65o6RVv6WGaDLR0NPa4jywQ/Vb60WHcKhqWDDTob/Y0Yj1MJ\nZbWHEh2Ks7lr61QeOCqb56nGgqJSvh37uB/NAZReC5ifjpOkkp1wMZddx2a40m8r\nvxCm1UsCMV1uwoPi9jo2NVoSUEk2AOkJQ9kO9IZI4VEHh0d66Ivr9YCYbLrcSnjv\nwBBWFu9E32kbg9sZz91kJeDEQ4un+ubRkXEOzINTb3DTOG52rAGd176wK6vKMrUt\naEVZXSWJAgMBAAECggEATZXXZIE5GxGBsPkDQF8SUVRd8onRjyA2tW3nc/TttdjQ\n0T0f78Np5LrqlbGzGOOaTa+y2MedCr/sSVYoZ4yYqDtFhuWWF/Zw1vNGcX51vi1R\n3NAc64wisMqgKIe+0Pw+d/fOsAQEQLzR49pdxGTU2K1gb9zYBiea0HBMmF03ZT/H\ndrTK5SBMKli4QLtyLdWlf3SeVZPjlYtViF7R8LUTtAl/RK7o/j4SVU89gB9nhu8S\nJavfEqOLhOvjMD5oBKqkN65DtTt9PQoltGyQf6E9LMyoU1TLYMqO8nK/L3DeSemb\nHyesTrot36DahI6hOGruxh5nZeKtDL0olW9zioHxFQKBgQD6DjHD+mOGOXugR59A\nOOuBKW9wKwF3Q6weQ/pBjh6qOtDArX8+7myX8XGwuEIWQnB/WHxg2AZMoGpimXTA\n27npV8jvhtwbpMm4oH/Dhi6yGbFLv4MIHqMnzqLtCrl7v5rSeRXOj6niRulsQTNd\nzu3XGVvoarpkj5xaCBR8JM4I5wKBgQDW4orap+axneEs/In2YWS7XCKrT/Vh1jFZ\nCPKCsqQLvikLcCIqHKbLWfTzZ9jg7sf/X2iTRdW9CkC368HOzDbbnokqmZbEbieK\n0ijiERn42/0ELUsN0247LfPL9PiMmNry0+dwwuO5c5eXGKjzHknbayQJNDojICUe\n75nsX+xgDwKBgGCgjZCKuFzM573nD1304OSXi22P4NAigWdso08jySvE8JgUM0Cx\nR5trfKBqg3cwk2JSYjXWJJEFyDZz0E8aWXwXJRmSRooUIrNKqjR3LKI8FWwNBGhl\n1mxyiDkLVOPCBeThT5DzqRU8i8T+aQgOB5CARyjH3WEwsZ2E082pN0jvAoGAetGM\n76DbuFU2qJHHAO11t6jXn/bZl4vnNK5qfc1A+f08VG9I96zcoInP+Xc3fIi+AvYz\nPUnC+oNrrjOv2FTcXfZlC9YxWCBltXCdMWj44UZLfSGuLj7UWDrRh5eCoEmViwkW\n1Vpu8kZhNFlB54M+T40TUbdqKz1qOh5UacgBwH0CgYEAmK4gPDC9D9lBlSWAwAGD\nsvAILMTRcraLPD2TujL8Oo39AhofIBLCLCN95TNIBCic+9XrUs85BejuQLp/bMMP\n3A0aLyxVjTzrLGFD6mjD4gAE+Oyf0deloxcUJJimO8yjAQPjXwHrkYo7CUsJveDb\nHllHrrK5EmGbdIsNUteuIbI=\n-----END PRIVATE KEY-----\n",
    "client_email": "firebase-adminsdk-6prfn@mobileprojectutp.iam.gserviceaccount.com",
    "client_id": "111646007081938487221",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-6prfn%40mobileprojectutp.iam.gserviceaccount.com"
};

// Firebase init
firebase.initializeApp(firebaseConfig);
admin.initializeApp({
    credential: admin.credential.cert(adminConfig),
    databaseURL: "https://mobileprojectutp-default-rtdb.firebaseio.com",
    storageBucket: "gs://mobileprojectutp.appspot.com"
});

// Database refs
const databaseRef = firebase.database().ref();
const postsRef = databaseRef.child('posts');

// Storage ref
const bucket = admin.storage().bucket();

// Get users list
function getUsersList() {
    var users = [];
    admin.auth().listUsers(1000)
        .then((listUsersResult) => {
            listUsersResult.users.forEach((userRecord) => {
                users.push(userRecord.toJSON());
            });
        }).catch((error) => {
            console.log('Błąd pobierania użytkowników:', error);
        });
    return users;
}

// Get posts list
function getPostsList() {
    var posts_list = [];
    postsRef.on("value", result => {
        result.forEach(post => {
            var jsonObj = post.toJSON();
            jsonObj.id = post.key;
            posts_list.push(jsonObj);
        });
    });
    return posts_list;
}

module.exports = {
    users: getUsersList(),
    posts_list: getPostsList(),
    firebase: firebase,
    admin: admin,
    bucket: bucket,
    postsRef: postsRef
};